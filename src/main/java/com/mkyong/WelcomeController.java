package com.mkyong;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/")
public class WelcomeController {

	// inject via application.properties

	@RequestMapping("/modifybillingtariff")
	public String firstPage() {
		return "modifybillingtariff";
	}

	@ModelAttribute("countries")
	public void countryList(Model model) {
		List<String> countries = Arrays.asList("India", "Pakisthan", "Srilanka");
		model.addAttribute("countries", countries);
	}

	@RequestMapping(value = "/loadStates", headers = "Accept=*/*", method = RequestMethod.GET)
	public @ResponseBody List<String> loadStates(@RequestParam(value = "countryId", required = true) String countryId)
			throws IllegalStateException {
		Map<String, List<String>> map = new HashMap<>();
		map.put("India", Arrays.asList("Assam", "WestBengal", "J&K", "Tamil Nadu", "Karnataka"));
		map.put("Pakisthan", Arrays.asList("Lahore", "Abbotabad", "Karachi", "Sind", "Islamabad"));
		map.put("Srilanka", Arrays.asList("Gaul", "Colombo", "Kandy"));
		return map.get(countryId);
	}

	@ModelAttribute("countrydetails")
	public CountryDetails countryDetails() {
		return new CountryDetails();
	}

	@RequestMapping(value = "/addNewProduct", method = RequestMethod.POST)
	public void processRequest(@ModelAttribute("countrydetails") CountryDetails countryDetails) {
		System.out.println(countryDetails);
	}

	@RequestMapping(value = "/loadDistricts", headers = "Accept=*/*", method = RequestMethod.GET)
	public @ResponseBody List<String> loadDistricts(
			@RequestParam(value = "countryId", required = true) String countryId) throws IllegalStateException {
		Map<String, List<String>> map = new HashMap<>();
		map.put("India", Arrays.asList("Durgapur", "Rajarhat", "Saltlake", "Asansol", "Birbhum"));
		map.put("Pakisthan", Arrays.asList("Muzaffarabad", "Kotli", "Bagh", "Haveli", "Neelum"));
		map.put("Srilanka", Arrays.asList("Ampara", "Jafna", "Galle"));
		return map.get(countryId);
	}

}