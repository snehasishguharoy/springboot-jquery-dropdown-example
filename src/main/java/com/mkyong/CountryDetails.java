package com.mkyong;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class CountryDetails {

	private  String countryId;
	private  String stateId;
	private  String districtId;

}
