<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>JSP Page</title>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
</head>

<script>
	$(document)
			.ready(
					function() {

						$('#countryId')
								.change(
										function() {

											$
													.getJSON(
															'loadStates',
															{
																countryId : $(
																		this)
																		.val(),
																ajax : 'true'
															},
															function(data) {
																console
																		.log(data);
																var html = '<option value="">----Select State----</option>';
																var len = data.length;

																for (var i = 0; i < len; i++) {
																	html += '<option value="' + data[i] + '">'
																			+ data[i]
																			+ '</option>';
																}
																html += '</option>';

																$('#stateId')
																		.html(
																				html);
															});
										});

					});

	$(document)
			.ready(
					function() {

						$('#countryId')
								.change(
										function() {

											$
													.getJSON(
															'loadDistricts',
															{
																countryId : $(
																		this)
																		.val(),
																ajax : 'true'
															},
															function(data) {
																console
																		.log(data);
																var html = '<option value="">----Select District----</option>';
																var len = data.length;

																for (var i = 0; i < len; i++) {
																	html += '<option value="' + data[i] + '">'
																			+ data[i]
																			+ '</option>';
																}
																html += '</option>';

																$('#districtId')
																		.html(
																				html);
															});
										});

					});
</script>
<body>

	<h3>Countries</h3>
	<form action="addNewProduct" method="post"
		modelAttribute="countrydetails">
		<table class="data">

			<tr>
				<td>Country-Name</td>
				<td><select id="countryId" name="countryId" name="countryId">
						<option value="">Select Country</option>
						<c:forEach items="${countries}" var="country">
							<option value="${country}">${country}</option>
						</c:forEach>
				</select>
				</td>
			</tr>
			<tr>
				<td>State-Name</td>
				<td><select id="stateId" name="stateId">
						<option value="">Select State</option>
				</select></td>
			</tr>
			<tr>
				<td>District-Name</td>
				<td><select id="districtId" name="districtId">
						<option value="">Select District</option>
				</select></td>
			</tr>
		</table>
		<input type="SUBMIT" value="Submit" />
	</form>

</body>



</html>
